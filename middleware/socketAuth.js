
module.exports = async (socket, next) => {
    const { user } = socket.handshake.auth;
    try {
        socket.data = { user: user };
        next();
    } catch (error) {
        console.log(error);
        next(error);
    }
}