import { Avatar, IconButton } from "@mui/material";
import styled from "styled-components";
import MoreVertIncon from '@mui/icons-material/MoreVert';
import AttachFileIcon from '@mui/icons-material/AttachFile';
import InsertEmoticonIcon from "@mui/icons-material/InsertEmoticon";
import MicIcon from '@mui/icons-material/Mic';
import { useState, useRef } from 'react'
import { useSelector,useDispatch } from '../store/store';
import { getSignalProtocolManagerUser, getSocket, getChats } from '../store/slices/signalSlice';
import { useEffect } from "react";
import { useSession } from "next-auth/react"
import Message  from "../components/Message";


function ChatScreen({ recipient }) {

    const { data: session } = useSession();
    const [input, setInput] = useState("");
    const endOfMessageRef = useRef(null);
    const signalProtocolManagerUser = useSelector(getSignalProtocolManagerUser)
    const socket = useSelector(getSocket);
    const chats = useSelector(getChats);
    const [messages, setMessages] = useState([]);
    const dispatch = useDispatch();

    const scrollToBottom = () => {
        endOfMessageRef.current.scrollIntoView({
            behavior: "smooth",
            block: "start",
        });
    };

    const ShowMessages = () => {
        return messages.map((message) => (
            <Message
                key={message.messageId}
                user = {message.senderid}
                message={message}
            />
        ))
    }

    const getChat=()=>{
        return localStorage.getItem(`${session.user.id}_messages`) ? JSON.parse(localStorage.getItem(`${session.user.id}_messages`)) : {};
    }

    const getSelectedUserChatId = () => {
        let selectedUserChatId = undefined;
        let localChats= getChat();
        if (localChats) {
            for (let chat of Object.values(localChats)) {
                if (chat.members.includes(recipient?.id)) {
                    selectedUserChatId = chat.chatId;
                    break;
                }
            }
        }
        return selectedUserChatId;
    }


    const sendMessage = async (e) => {
        e.preventDefault();
        localStorage.setItem('LastSentMessage', input);
        let chatId = getSelectedUserChatId();
        let message = {
            message: input,
            timestamp: new Date(),
        }
        let messageBody = {
            chatId: chatId,
            senderid: session?.user?.id,
            receiverid: recipient?.id,
            ...message
        }
        try {
            let ecryptmessage = await signalProtocolManagerUser.encryptMessageAsync(recipient.id, message.message);
            messageBody.message = ecryptmessage;
            // console.log(socket);
            socket.emit('send-message', messageBody);
        } catch (error) {
            console.log(error);
        }

        setInput("");
        scrollToBottom();
    };


    useEffect(() => {
        if (chats && session) {
            let selectedChatId = chats[getSelectedUserChatId()];
            if (selectedChatId) {
                setMessages(selectedChatId.messages);
            } else {
                setMessages([]);
            }
            // scrollToBottom();
        }
      
    }, [chats,session])


    const selectedChatByUserChatId = (id,chats) => {
        let selectedUserChatId = undefined;
        if (chats) {
            for (let chat of Object.values(chats)) {
                if (chat.members.includes(id)) {
                    selectedUserChatId = chat.chatId;
                    break;
                }
            }
        }
        return selectedUserChatId;
    }

    useEffect(() => {
        if (recipient && session) {
            // dispatch(setMessageToUser(recipient));
            let lsChats = getChat();
            let selectedChatId = lsChats[selectedChatByUserChatId(recipient?.id,lsChats)];
            if (selectedChatId) setMessages(selectedChatId.messages);
            else setMessages([]);
            // scrollToBottom();
        }
    }, [recipient,session])


    return (
        <Container>
            <Header>

                {recipient ? (
                    <Avatar src={recipient?.image} />
                ) : <Avatar>
                    {recipient?.name[0]}
                </Avatar>}

                <HeaderInformation>
                    <h3>{recipient?.email}</h3>
                    {/* {recipientSnapshot ? (
                        <p>Last active: {' '}
                            {recipient?.lastSeen?.toDate() ? (
                                <TimeAgo datetime={recipient?.lastSeen?.toDate()} />
                            ) : "Unavailable"}
                        </p>
                    ) : (
                        <p>Loading Last active...</p>
                    )} */}
                </HeaderInformation>
                <HeaderIcons>
                    <IconButton>
                        <AttachFileIcon />
                    </IconButton>
                    <IconButton>
                        <MoreVertIncon />
                    </IconButton>
                </HeaderIcons>
            </Header>

            <MessageContainer>
                {ShowMessages()}
                <EndOfMessage ref={endOfMessageRef} />
            </MessageContainer>

            <InputContainer>
                <InsertEmoticonIcon />
                <Input value={input} onChange={e => setInput(e.target.value)} />
                <button hidden disabled={!input} type="submit" onClick={sendMessage}>Send Message</button>
                <MicIcon />
            </InputContainer>
        </Container>
    )

}


export default ChatScreen;

const Container = styled.div``;


const Input = styled.input`
    flex: 1;
    outline: 0;
    border: none;
    border-radius: 10px;
    background-color: whitesmoke;
    padding: 20px;
    margin-left: 15px;
    margin-right: 15px;
    
`;

const InputContainer = styled.form`
    display: flex;
    align-items: center;
    padding: 10px;
    position: sticky;
    bottom: 0;
    background-color: white;
    z-index: 100;
`;

const Header = styled.div`
    position: sticky;
    background-color: white;
    z-index: 100;
    top: 0;
    display: flex;
    padding: 11px;
    height: 80px;
    align-items: center;
    border-bottom: 1px solid whitesmoke;
`;

const HeaderInformation = styled.div`
    margin-left: 15px;
    flex: 1;

    > h3 {
        margin-bottom: 3px;
    }

    > p {
        font-size: 14px;
        color: gray;
    }
`;


const EndOfMessage = styled.div`
    margin-bottom: 50px;
`;

const HeaderIcons = styled.div``;

const MessageContainer = styled.div`
    /* padding: 30px; */
    background-image: url('https://user-images.githubusercontent.com/15075759/28719144-86dc0f70-73b1-11e7-911d-60d70fcded21.png');
    /* background-color: #e5ded8; */
    min-height: 90vh;
    flex: 1;
    background-image: url('https://user-images.githubusercontent.com/15075759/28719144-86dc0f70-73b1-11e7-911d-60d70fcded21.png');
    background-repeat: repeat;
    background-position: center;
    padding: 30px;
    /* overflow:scroll; */
`;

