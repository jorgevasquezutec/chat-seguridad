
import { Avatar, IconButton, Button } from "@mui/material";
import ChatIcon from '@mui/icons-material/Chat';
import MoreVertIncon from '@mui/icons-material/MoreVert';
import SearchIcon from '@mui/icons-material/Search';
import styled from "styled-components"
import { signOut } from "next-auth/react"
import Chat from "../components/Chat"
import { useSession } from "next-auth/react"
import { useEffect, useState } from "react";
import { getContactsAPI, loginAPI } from "../services/api"
import { setSignalProtocolManagerUser, setSocket, getSignalProtocolManagerUser, setChats } from '../store/slices/signalSlice';
import { useDispatch, useSelector } from '../store/store';
import Head from 'next/head'
import io from 'socket.io-client';


function Sidebar() {
    const [contacts, setContacts] = useState([]);
    const { data: session } = useSession()
    const signalProtocolManagerUser = useSelector(getSignalProtocolManagerUser)
    const dispatch = useDispatch();

    const onInit = async () => {
        await getContacts(session.user.id);
        await socketInitializer(session.user.id);
    }

    const getContacts = async (id) => {
        const contacts = await getContactsAPI(id);
        setContacts(contacts.data);
    }

    const handleKeyBundle = async (session) => {
        const { SignalServerStore } = await import('../signal/SignalGateway');
        const { createSignalProtocolManager } = await import('../signal/SignalGateway');
        const dummySignalServer = new SignalServerStore();
        await loginAPI(session.user)
        const signalProtocolManagerUser = await createSignalProtocolManager(session.user.id, session.user.email, dummySignalServer);
        dispatch(setSignalProtocolManagerUser(signalProtocolManagerUser));
        // await socketInitializer();
        return signalProtocolManagerUser;

    }
    const socketInitializer = async (id) => {
        const userId= id;
        // console.log(userId)
        const signalProtocolManagerUserLocal = await handleKeyBundle(session);
        console.log('startManager', signalProtocolManagerUserLocal)
        await fetch('/api/socket');
        let socket = io({
            auth: {
                user: session.user,
            }
        })
        dispatch(setSocket(socket));

        socket.on('connection', () => {
            console.log('connected')
        })

        socket.on('receive-message', async (message) => {
            console.log('signalProtocolManagerUserLocal', signalProtocolManagerUserLocal)
            let new_message = message;
            console.log("receiveCliente", message);
            let tmpChats = localStorage.getItem(`${session.user.id}_messages`);
            if (tmpChats) tmpChats = JSON.parse(tmpChats);

            if (new_message.senderid === session.user.id) {
                new_message.message = localStorage.getItem('LastSentMessage');
            } else {
                let decrypt_message = await signalProtocolManagerUserLocal.decryptMessageAsync(new_message.senderid, new_message.message);
                new_message.message = decrypt_message;
            }

            console.log("new_message", new_message);

            // console.log("tmpChats", tmpChats);

            if (tmpChats && new_message.chatId in tmpChats) {
                const ochats = {
                    ...tmpChats,
                    [new_message.chatId]: {
                        ...tmpChats[new_message.chatId],
                        messages: [...tmpChats[new_message.chatId].messages.concat(new_message)]
                    }
                }
                dispatch(setChats(ochats)); // REVISAR SI ES NECESARIO
                localStorage.setItem(`${userId}_messages`, JSON.stringify(ochats));
            }
            else {
                const newChat = {
                    chatId: new_message.chatId,
                    members: [new_message.senderid, new_message.receiverid],
                    messages: []
                }
                newChat.messages.push(new_message)
                const newOchat = {
                    ...tmpChats,
                    [new_message.chatId]: newChat
                }
                dispatch(setChats(newOchat));
                localStorage.setItem(`${userId}_messages`, JSON.stringify(newOchat));
            }

        })
    }

    useEffect(() => {
        if (session) {
            onInit();
        }
    }, [session])



    return (
        <Container>
            <Head>
                <script type="text/javascript" src="/libsignal-protocol.js"></script>
            </Head>
            <Header>
                <UserAvatar src={session?.user?.image} onClick={() => signOut()} />
                <p>{session?.user?.id}</p>
                <IconsContainer>
                    <IconButton>
                        <ChatIcon />
                    </IconButton>
                    <IconButton>
                        <MoreVertIncon />
                    </IconButton>
                </IconsContainer>
            </Header>
            <Search>
                <SearchIcon />
                <SearchInput placeholder="Search in chats" />
            </Search>
            {/* List of contacts */}
            {
                contacts?.map((contact) => (
                    <Chat key={contact.id} id={contact.id} name={contact.email} user={contact} />
                ))
            }

        </Container>
    )
}

const Container = styled.div`
    flex: 0.45;
    border-right: 1px solid whitesmoke;
    height: 100vh;
    min-width: 300px;
    max-width: 350px;
    overflow-y: scroll;
    ::-webkit-scrollbar {
        display: none;
    }
    -ms-overflow-style: none;
    scrollbar-width: none;
`;

const Search = styled.div`
    display: flex;
    align-items: center;
    padding: 20px;
    border-radius: 2px;
`;

const SidebarButton = styled(Button)`
    width: 100%;
    &&& {
    border-top : 1px solid whitesmoke;
    border-bottom : 1px solid whitesmoke;
    }
`;

const SearchInput = styled.input`
    outline-width: 0;
    border: none;
    flex: 1;
`;

const Header = styled.div`
    display: flex;
    position: sticky;
    top : 0;
    background-color: white;
    z-index: 1;
    justify-content: space-between;
    align-items: center;
    padding: 15px;
    height: 80px;
    border-bottom: 1px solid whitesmoke;
`;

const UserAvatar = styled(Avatar)`
    cursor: pointer;

    :hover {
        opacity: 0.8;
    }
`

const IconsContainer = styled.div``;

export default Sidebar;