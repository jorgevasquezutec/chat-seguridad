import Sidebar from "./Sidebar"
import styled from "styled-components"

export default function Layout({ children }) {
    return (
        <Container>
            <Sidebar />
            <Main>{children}</Main>
        </Container>
    )
}



const Container = styled.div`
  display: flex;
  flex-direction: row;
  `

const Main = styled.main`
    flex: 1;
`
