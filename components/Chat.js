

import styled from "styled-components";
import { Avatar } from "@mui/material";
import Link from "next/link";



function Chat({ id,name,user }) {
    return (
        <Container>
            <DLink href={`/chat/${id}`}>
                <UserAvatar src={user?.image} />
                <p>{name}</p>
            </DLink>
        </Container>
    )


}


export default Chat;

const DLink = styled(Link)`
    display: flex;
    flex-direction: row;
`

const Container = styled.div`
    display: flex;
    align-items: center;
    padding: 15px;
    word-break: break-word;
    cursor: pointer;
    :hover {
        background-color: #e9eaeb;
    }
`;

const UserAvatar = styled(Avatar)`
    margin: 5px;
    margin-right: 15px;
`;