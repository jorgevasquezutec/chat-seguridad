
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
    signalProtocolManagerUser: undefined,
    socket: undefined,
    users: [],
    chats: {},
    lastSentMessage: undefined,
    msgText: "",
}

export const signalSlice = createSlice({
    name: 'signal',
    initialState,
    reducers: {
        setSignalProtocolManagerUser: (state, action) => {
            state.signalProtocolManagerUser = action.payload;
        },
        setSocket: (state, action) => {
            state.socket = action.payload;
        },
        setUsers: (state, action) => {
            state.users = action.payload;
        },
        setChats: (state, action) => {
            state.chats = action.payload;
        },
        setLastSentMessage: (state, action) => {
            state.lastSentMessage = action.payload;
        },
        setMsgText: (state, action) => {
            state.msgText = action.payload;
        }
    },
});

// A small helper of user state for `useSelector` function.
export const getSignalProtocolManagerUser = (state) => state.signal.signalProtocolManagerUser;
export const getSocket = (state) => state.signal.socket;
export const getUsers = (state) => state.signal.users;
export const getChats = (state) => state.signal.chats;
export const getLastSentMessage = (state) => state.signal.lastSentMessage;
export const getMsgText = (state) => state.signal.msgText;

// Exports all actions
export const {
    setSignalProtocolManagerUser,
    setSocket,
    setUsers,
    setChats,
    setLastSentMessage,
    setMsgText,
} = signalSlice.actions;

export default signalSlice.reducer;