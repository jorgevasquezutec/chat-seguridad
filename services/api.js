import axios from 'axios';


const header = {
    headers: {
        'Content-Type': 'application/json',
    }
}

export const  loginAPI = async (user) => {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await axios.post('/api/users', user, header);
            resolve(response);
        } catch (error) {
            reject(error);
        }
    })
}

export const  getPreKeyBundleAPI = async (userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await axios.get(`/api/users/${userId}`);
            resolve(response.data.data.key);
        } catch (error) {
            reject(error);
        }
    })
}

export const getUserByIdAPI = async (userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await axios.get(`/api/users/${userId}`);
            resolve(response.data);
        } catch (error) {
            reject(error);
        }
    })
}


export const  setPreKeyBundleAPI = async (userId, storageBundle) => {
    return new Promise(async (resolve, reject) => {
        try {
            await axios.put(`/api/users/${userId}`, {
                key: storageBundle
            }, header);
            resolve();
        } catch (error) {
            reject(error);
        }
    })
}

export const existsPreKeyBundleAPI = async (userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            const storageBundle = await getPreKeyBundleAPI(userId);
            resolve(!!storageBundle?.data);
        } catch (error) {
            reject(error);
        }
    })
}


export const getContactsAPI = async (userId) => {
    return new Promise(async (resolve, reject) => {
        try {
            const response = await axios.get(`/api/contacts/${userId}`);
            resolve(response.data);
        } catch (error) {
            reject(error);
        }
    })
}


export const validToken = async (token,csrf) => {
    try {
        const response = await axios.get('/api/auth/session',{
            headers: {
                'Set-Cookie': `next-auth.csrf-token=${csrf}; next-auth.callback-url=${process.env.NEXTAUTH_URL}; next-auth.session-token=${token};`
            }
        });
        return response;
    } catch (error) {
        throw error;
    }
}
