import mongoose from 'mongoose';


const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    image : {
        type: String,
        required: false,
    },
    email: {
        type: String,
        required: true,
    },
    id: {
        type: String,
        required: true,
    },
    key : {
        type: Object,
        required: false,
    }
});


module.exports = mongoose.models?.User || mongoose.model('User', userSchema)