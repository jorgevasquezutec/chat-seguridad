import dbConnect from '../../../lib/dbConnect'
import User from '../../../models/User'

export default async function handler(req, res) {
    const { method } = req
    await dbConnect();
    switch (method) {
        case 'GET':
            try {
                const users = await User.find({
                    id: { $ne: req.query.id }
                });
                res.status(201).json({ success: true, data: users })
            } catch (error) {
                res.status(400).json({ success: false, error: error })
            }
            break
        default:
            res.status(400).json({ success: false })
            break
    }
}