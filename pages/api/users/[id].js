import dbConnect from '../../../lib/dbConnect'
import User from '../../../models/User'


export default async function handler (req, res) {
    const { method } = req
    await dbConnect();
    switch (method) {
        case 'PUT':
            try {
                let user = await User.findOne({
                    id: req.query.id
                });
                user.key = req.body.key;
                await user.save();
                res.status(201).json({ success: true, data: user })
            } catch (error) {
                res.status(400).json({ success: false, error: error })
            }
            break
        case 'GET':
            try {
                const user = await User.findOne({ id: req.query.id });
                console.log(user);
                res.status(201).json({ success: true, data: user })

            } catch (error) {
                res.status(400).json({ success: false, error: error })
            }
            break
        default:
            res.status(400).json({ success: false })
            break

    }
};