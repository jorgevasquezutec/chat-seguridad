import NextAuth from "next-auth"
import GoogleProvider from "next-auth/providers/google";
import axios from "axios";

// import dbConnect from "../../../lib/dbConnect";
// import User from "../../../models/User";

export const authOptions = {
  // Configure one or more authentication providers
  providers: [
    GoogleProvider({
      clientId: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET
    })
  ],
  secret: process.env.JWT_SECRET,
  callbacks: {
    async signIn({ user, account, profile, email, credentials }) {

      return true
    },
    async session({ session, token, user }) {
      // console.log(session,token,user)
      session.user.id = token.sub;
      session.accessToken = token.accessToken;
      return session
    },
    async redirect({ url, baseUrl }) {
      // console.log(url,baseUrl)
      return Promise.resolve(baseUrl)
    },
    async jwt({ token, user, account, profile, isNewUser }) {
      if (user) {
        token.id = user.id;
      }
      if (account) {
        token.accessToken = account.access_token;
      }
      return token;
    }

  }
}
export default NextAuth(authOptions)