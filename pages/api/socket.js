import { Server } from 'socket.io'
import SocketAuth from '../../middleware/socketAuth'
import { v4 as uuidv4 } from 'uuid';


const SocketHandler = async (req, res) => {
  if (res.socket.server.io) {
    console.log('Socket is already running')
  } else {
    console.log('Socket is initializing')
    const io = new Server(res.socket.server)
    res.socket.server.io = io;
    io.use(SocketAuth);
    io.on("connection", socket => {
      const user = socket.data.user;
      console.log("Client connected", user);
      socket.join(user.id);
   
      socket.on("disconnect", function () {
        console.log("Client disconnected");
        socket.leave(user.id);
      });

      socket.on("send-message", function (message) {
        console.log("Message received", message);
        if (message.chatId === undefined) {
          message.chatId = uuidv4();
        }
        message.messageId = uuidv4();
        io.to(message.receiverid).emit("receive-message", message);
        io.to(message.senderid).emit("receive-message", message); // puedo omitirlo
      })


    });
  }
  res.end()
}

export default SocketHandler