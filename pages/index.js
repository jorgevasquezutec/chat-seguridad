import Head from 'next/head'
import { getSession } from 'next-auth/react'

function Home({ osession }) {

  return (
    <div>
      <Head>
        <title>Seguridad Chat</title>
        <meta name="description" content="Chat" />
        <meta name="referrer" content="no-referrer" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
    </div>
  )
}

export async function getServerSideProps(context) {
  const session = await getSession(context);
  if (!session) return {
    redirect: {
      destination: '/api/auth/signin/google',
      permanet: false
    }
  }
  return {
    props: {
      osession: session,
    }
  }
}

export default Home;