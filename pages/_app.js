import '../styles/globals.css'
import { SessionProvider } from "next-auth/react"
import { Provider } from 'react-redux';
import { store } from '../store/store';
import Layout from '../components/Layout';


export default function App({
  Component,
  pageProps: { session, ...pageProps },
}) {

  return (
    <Provider store={store}>
      <SessionProvider session={session}>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </SessionProvider>
    </Provider>
  )
}