
import Head from "next/head";
import styled from "styled-components"
// import Sidebar from "../../components/Sidebar";
import { getUserByIdAPI } from "../../services/api"
import { useState, useEffect } from "react";
import { useRouter } from 'next/router'
import ChatScreen from "../../components/ChatScreen";

function Chat() {
    const router = useRouter();
    const [user, setUser] = useState(null);


    const getUser = async (id) => {
        const user = await getUserByIdAPI(id);
        setUser(user.data);
    }

    useEffect(() => {  
        if(router?.query?.id){
            getUser(router.query.id);
        }
    }, [router])


    return (
        <Container>
            {/* <Head>
                <title>Chat with {user?.name} </title>
            </Head> */}
            {/* <Sidebar /> */}
            <ChatContainer>
                <ChatScreen recipient={user} />
            </ChatContainer>
        </Container>
    )
}

export default Chat;

const Container = styled.div`
    display: flex;
`

const ChatContainer = styled.div`
    flex: 1;
    overflow: scroll;
    height: 100vh;
    ::-webkit-scrollbar {
        display: none;
    }
    -ms-overflow-style: none;
    scrollbar-width: none;`
